package controllers

import play.api._
import play.api.mvc._

import play.api.data._
import play.api.data.Forms._

import utils._

import models._
import com.novus.salat.dao.SalatMongoCursor

object Application extends Controller with IsAuthenticated{
  val postForm = Form(
    tuple(
      "content" -> text,
      "tags" -> text
    )
  )

  private[this] def displayPosts(cursor: SalatMongoCursor[Post]) =
    Action { implicit request =>
      val postItems = cursor.limit(10).toSeq
      Ok(views.html.posts(postItems, postForm, user.isDefined))
    }


  def posts = displayPosts(Post.all)

  def postsWithTag(tag: String) = displayPosts(Post.tagged(tag))

  def postsFromUser(user: String) = displayPosts(Post.fromUser(user))

  def index = Action{
    Redirect(routes.Application.posts)
  }

  def submit = ForceAuth{ user =>
    Action{ implicit request =>
      postForm.bindFromRequest.fold(
        error => BadRequest("fill out the from please"),
        post => {
          val content = post._1
          val tags = post._2.split(" *, *")
          Post.dao.insert(Post(content = content, user = user, tags = tags))
          Redirect(routes.Application.posts)
        }
      )
    }
  }

}