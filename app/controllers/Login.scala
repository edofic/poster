package controllers

import play.api._
import play.api.mvc._

import play.api.data._
import play.api.data.Forms._
import models.User


/**
 * User: andraz
 * Date: 12/6/12
 * Time: 10:12 PM
 */
object Login extends Controller{
  val loginForm = Form(
    tuple(
      "user" -> text,
      "password" -> text
    )
  )

  def login = Action { implicit request =>
    Ok(views.html.login(loginForm))
  }

  def logout = Action{ implicit request =>
    Redirect(routes.Login.login).withSession(
      session - "user"
    )
  }

  def register = Action{
    Ok(views.html.register(loginForm))
  }

  def completeRegistration = Action{ implicit request =>
    val (username, pass) = loginForm.bindFromRequest.get
    if (User.find(username).isDefined){
      Ok(views.html.register(loginForm, "user already exists"))
    } else {
      User.create(username, pass)
      Redirect(routes.Application.index).withSession(
        session + ("user" -> username)
      )
    }
  }

  def authenticate = Action{ implicit request =>
    val (username, pass) = loginForm.bindFromRequest.get
    val valid = User.find(username) map (_.password.authenticate(pass)) getOrElse(false)
    val redirect =
      if(valid) {
        Redirect(
          session.get("redirectOnLogin").getOrElse(routes.Application.index.url)
        )
      } else {
        Ok(views.html.login(loginForm, "wrong username/password"))
      }

    redirect.withSession(
      session + ("user" -> username) - "redirectOnLogin"
    )
  }
}
