package utils

import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec
import java.security.SecureRandom
import models.EncryptedPassword


/**
 * User: andraz
 * Date: 12/8/12
 * Time: 4:24 PM
 */
object Passwords {
  private[this] val algorithm = "PBKDF2WithHmacSHA1"

  def encrypt(plaintext: String) ={
    val salt = generateSalt()
    val hash = hashPassword(plaintext, salt)
    EncryptedPassword(hash, salt)
  }

  def authenticate(attempt: String, hash: Array[Byte], salt: Array[Byte]) = {
    val attemptHash = hashPassword(attempt, salt)
    attemptHash sameElements hash
  }

  def hashPassword(password: String, salt: Array[Byte]) = {
    val spec = new PBEKeySpec(password.toCharArray, salt, 10000, 160)
    val factory = SecretKeyFactory.getInstance(algorithm)
    factory.generateSecret(spec).getEncoded
  }

  def generateSalt() = {
    val random = SecureRandom.getInstance("SHA1PRNG")
    val salt = new Array[Byte](8)
    random.nextBytes(salt)
    salt
  }

}
