package utils

import play.api._
import play.api.mvc._
import controllers.routes

/**
 * User: andraz
 * Date: 11/3/12
 * Time: 10:51 AM
 */
trait IsAuthenticated { this: Controller =>

  private[this] def username(header: RequestHeader): Option[String] = header.session.get("user")
  private[this] def onUnauthorized(header: RequestHeader): Result =
    Redirect(routes.Login.login).withSession(
      header.session + ("redirectOnLogin" -> header.path)
    )

  def ForceAuth[A](f: String => Action[A]) =
    Security.Authenticated(username, onUnauthorized)(f)

  def user(implicit request: Request[_]) = username(request)
}
