package models

import play.api.Play.current
import com.novus.salat._
import com.novus.salat.dao._
import com.novus.salat.global._
import com.mongodb.casbah.Imports._
import se.radley.plugin.salat._

/**
 * User: andraz
 * Date: 12/6/12
 * Time: 8:04 AM
 */
case class Post(_id: ObjectId = new ObjectId,
                content: String,
                user: String,
                tags: Seq[String]
                )

object Post{
  object dao extends SalatDAO[Post, ObjectId](collection = mongoCollection("posts"))

  def all =
    dao.find(MongoDBObject()).sort(MongoDBObject("_id" -> -1))

  def tagged(tag: String) = dao.find(MongoDBObject("tags" -> tag)).sort(MongoDBObject("_id" -> -1))

  def fromUser(user: String) = dao.find(MongoDBObject("user" -> user)).sort(MongoDBObject("_id" -> -1))
}
