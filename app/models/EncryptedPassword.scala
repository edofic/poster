package models

import utils.Passwords

/**
 * User: andraz
 * Date: 12/8/12
 * Time: 4:51 PM
 */
case class EncryptedPassword(hash: Array[Byte], salt: Array[Byte]){
  def authenticate(attempt: String) = Passwords.authenticate(attempt, hash, salt)
}
