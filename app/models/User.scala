package models

import play.api.Play.current
import com.novus.salat._
import com.novus.salat.dao._
import com.novus.salat.global._
import com.mongodb.casbah.Imports._
import se.radley.plugin.salat._
import utils.Passwords

/**
 * User: andraz
 * Date: 12/6/12
 * Time: 10:13 PM
 */
case class User(username: String, password: EncryptedPassword)

object User{
  object dao extends SalatDAO[User, ObjectId](collection = mongoCollection("users"))

  def create(username: String, password: String){
    dao insert User(username, Passwords.encrypt(password))
  }

  def find(username: String): Option[User] =
    dao findOne MongoDBObject("username" -> username)

}

